package com.github.yjgbg.emailServerNew
package utils

import javax.persistence.EntityManager
import scala.reflect.ClassTag

class JpaOps(id: Long) {
  def findById[A: ClassTag]: Option[A] = {
    val clazz = implicitly[ClassTag[A]].runtimeClass.asInstanceOf[Class[A]]
    val entity = Spring[EntityManager].find(clazz, id)
    Option(entity)
  }
}

package com.github.yjgbg.emailServerNew
package utils

import org.springframework.jdbc.core.{BeanPropertyRowMapper, JdbcTemplate}

import java.util
import scala.language.implicitConversions
import scala.reflect.ClassTag

class SQLOps(sql: String) {
  def execute(): Unit = Spring[JdbcTemplate].execute(sql)

  def query[A: ClassTag]: util.List[A] = Spring[JdbcTemplate].query(sql,
    new BeanPropertyRowMapper(implicitly[ClassTag[A]].runtimeClass.asInstanceOf[Class[A]]))

  def queryForObject[A: ClassTag]: A = Spring[JdbcTemplate].queryForObject(sql,
    new BeanPropertyRowMapper(implicitly[ClassTag[A]].runtimeClass.asInstanceOf[Class[A]]))

  def queryForCell[A: ClassTag]: A = Spring[JdbcTemplate].queryForObject(sql,
    implicitly[ClassTag[A]].runtimeClass.asInstanceOf[Class[A]])

  def queryForColumn[A: ClassTag]: util.List[A] = Spring[JdbcTemplate].queryForList(sql,
    implicitly[ClassTag[A]].runtimeClass.asInstanceOf[Class[A]])
}

package com.github.yjgbg.emailServerNew
package utils

import com.fasterxml.jackson.databind.ObjectMapper

import scala.reflect.ClassTag

object JacksonOps {
  class ToJson[A](a: A) {
    def toJson: String = Spring[ObjectMapper].writeValueAsString(a)
  }

  class FromJson(jsonString: String) {
    def fromJson[A: ClassTag]: A = Spring[ObjectMapper].readValue(jsonString,
      implicitly[ClassTag[A]].runtimeClass.asInstanceOf[Class[A]])
  }
}

package com.github.yjgbg.emailServerNew

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.scheduling.annotation.EnableScheduling

@EnableScheduling
@SpringBootApplication
class EmailServerNewApplication

object EmailServerNewApplication extends App {
  SpringApplication.run(classOf[EmailServerNewApplication], args: _*)
}
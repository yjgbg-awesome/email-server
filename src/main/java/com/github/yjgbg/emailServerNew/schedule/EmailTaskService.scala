package com.github.yjgbg.emailServerNew
package schedule

import com.github.yjgbg.emailServerNew.jpa.repo.EmailTaskRepo
import org.slf4j.{Logger, LoggerFactory}
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

import java.util.concurrent.Executor

@Component
class EmailTaskService {
  val log: Logger = LoggerFactory.getLogger(classOf[EmailTaskService])
  val executor: Executor = _.run() // 在当前线程进行操作，如果后续有需要性能优化，可以使用线程池

  @Scheduled(fixedDelay = 10 * 1000)
  def serve(): Unit = try {
    // 已重试3次则不再重试
    Spring[EmailTaskRepo]
      .findByFinishedEqualsAndRetryTimesLessThan(finished = false, 3)
      .forEach(entity => executor.execute(() => entity.sendMail))
  } catch {
    case e: Exception =>
      log.error("查询数据库失败")
      e.printStackTrace()
  }
}

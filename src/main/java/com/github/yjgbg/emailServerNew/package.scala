package com.github.yjgbg

import com.github.yjgbg.emailServerNew.utils.JacksonOps.{FromJson, ToJson}
import com.github.yjgbg.emailServerNew.utils.{JpaOps, SQLOps}

import scala.language.implicitConversions

package object emailServerNew {
  val Spring: utils.Spring.type = utils.Spring

  implicit def strToSqlOps(string: String): SQLOps = new SQLOps(string)

  implicit def toJson[A](a: A): ToJson[A] = new ToJson[A](a)

  implicit def fromJson[A](json: String): FromJson = new FromJson(json)

  implicit def JpaOps(id: Long): JpaOps = new JpaOps(id)

  implicit class ToOption[A](a: A) {
    def op: Option[A] = Option(a)
  }
}

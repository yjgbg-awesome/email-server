package com.github.yjgbg.emailServerNew
package jpa.entity

import com.github.yjgbg.emailServerNew.jpa.converters.EmailToSetConvertor
import com.github.yjgbg.emailServerNew.jpa.support.StdEntity
import org.springframework.boot.autoconfigure.mail.MailProperties
import org.springframework.mail.javamail.{JavaMailSender, MimeMessageHelper}

import java.time.LocalDateTime
import javax.persistence._

@Entity
@Access(AccessType.FIELD)
@Table(name = "email_task")
class EmailTaskEntity extends StdEntity {
  @Convert(converter = classOf[EmailToSetConvertor])
  @Column(name = "`to`")
  var to: Set[String] = Set()
  @Column(name = "`subject`")
  var subject: String = _
  @Column(name = "`text`")
  var text: String = _
  var finished: Boolean = false
  var finishedTime: LocalDateTime = _
  var retryTimes: Int = _

  def sendMail: this.type = try {
    val mimeMessage = Spring[JavaMailSender].createMimeMessage()
    mimeMessage.setFrom(Spring[MailProperties].getUsername)
    val helper = new MimeMessageHelper(mimeMessage)
    helper.setTo(to.toArray)
    helper.setText(text, true)
    // utf-8 qq邮箱空格问题
    mimeMessage.setSubject(subject, "gbk")
    Spring[JavaMailSender].send(mimeMessage)
    finished = true
    finishedTime = LocalDateTime.now()
    save
  } catch {
    case e: Exception =>
      e.printStackTrace()
      finished = false
      retryTimes += 1
      finishedTime = null
      save
  }
}

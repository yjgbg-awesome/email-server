package com.github.yjgbg.emailServerNew
package jpa.repo

import com.github.yjgbg.emailServerNew.jpa.entity.EmailTaskEntity
import com.github.yjgbg.emailServerNew.jpa.support.StdRepo

trait EmailTaskRepo extends StdRepo[EmailTaskEntity] {
  def findByFinishedEqualsAndRetryTimesLessThan(finished: Boolean, retryTime: Int): java.util.List[EmailTaskEntity]
}


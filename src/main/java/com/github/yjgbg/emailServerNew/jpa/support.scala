package com.github.yjgbg.emailServerNew
package jpa

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.NoRepositoryBean

import java.time.LocalDateTime
import javax.persistence._

object support {
  @NoRepositoryBean
  trait StdRepo[A <: StdEntity] extends JpaRepository[A, Long]

  @MappedSuperclass
  trait StdEntity extends Serializable with Comparable[StdEntity] {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = _

    var createTime: LocalDateTime = _

    def save: this.type = Spring.transactional(() => Spring[EntityManager].merge(this))

    def remove(): Unit = Spring.transactional(() => Spring[EntityManager].remove(this))

    override def compareTo(o: StdEntity): Int = this.id compareTo o.id
  }
}
package com.github.yjgbg.emailServerNew
package jpa

import javax.persistence.AttributeConverter

object converters {
  private val SEPARATOR = ";"

  class EmailToSetConvertor extends AttributeConverter[Set[String], String] {
    override def convertToEntityAttribute(attribute: String): Set[String] =
      if (attribute == null) Set() else attribute.split(SEPARATOR).toSet

    override def convertToDatabaseColumn(dbData: Set[String]): String = dbData.mkString(SEPARATOR)
  }

}
